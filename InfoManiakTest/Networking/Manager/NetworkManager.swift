//
//  NetworkManager.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

protocol NetworkManager {
    associatedtype MyRouter
    var router: MyRouter { get }
    func getModel<T : Decodable>(ofType: T.Type, endPoint: ITunesAPI, completion: @escaping (Result<T, NetworkErrorResponse>) -> ())
}

enum NetworkErrorResponse: Error {
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode
    
    var description: String {
        switch self {
        case .badRequest:
            return "Bad request"
        case .outdated:
            return "The url you requested is outdated."
        case .failed:
            return "Network request failed."
        case .noData:
            return "Response returned with no data fetch from database."
        case .unableToDecode:
            return "We could not decode the response."
        }
    }
}
