//
//  ITunesManager.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

struct ITunesManager: NetworkManager {
    let router = Router<ITunesAPI>()
    
    func getModel<T : Decodable>(ofType: T.Type, endPoint: ITunesAPI, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
        
        router.request(endPoint) { result in
            switch result {
            case .failure(_):
                completion(.failure(NetworkErrorResponse.failed))
            case .success(let data):
                do {
                    let _ = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(model))
                } catch (let error) {
                    print(error)
                    completion(.failure(NetworkErrorResponse.unableToDecode))
                }
            }
        }
    }
}
