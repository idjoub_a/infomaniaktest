//
//  ArtistModel.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

struct Artist: Codable {
    let resultCount: Int
    let results: [ArtistInfo]
}

struct ArtistInfo: Codable {
    let artistName: String
    let artistLinkURL: String
    let primaryGenreName: String

    enum CodingKeys: String, CodingKey {
        case artistName
        case artistLinkURL = "artistLinkUrl"
        case primaryGenreName
    }
}
