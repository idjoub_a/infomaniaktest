//
//  SongModel.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

struct Song: Codable {
    let resultCount: Int
    let results: [SongInfo]
}

struct SongInfo: Codable {
    let kind: String
    let artistName, collectionName, trackName: String
    let artworkUrl100: String

    enum CodingKeys: String, CodingKey {
        case kind
        case artistName, collectionName, trackName
        case artworkUrl100
    }
}
