//
//  iTunesAPI.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

//Artist only
//https://itunes.apple.com/search?term=ARTIST&entity=allArtist&attribute=allArtistTerm

//Track only
//https://itunes.apple.com/search?term=TRACK&entity=song&attribute=songTerm

enum ITunesAPI {
    case artistName(artistName: String)
    case trackName(trackName: String)
}

extension ITunesAPI: EndPointType {
    var method: HTTPMethod {
        return .get
    }

    var baseURL: String { return "https://itunes.apple.com/search?" }
    
    var parameters: [String : Any]? {
        switch self {
        case .artistName(let artistName):
            return ["term": artistName, "entity": "allArtist", "attribute": "allArtistTerm"]
        case .trackName(let trackName):
            return ["term": trackName, "entity": "song", "attribute": "songTerm"]
        }
    }
}
