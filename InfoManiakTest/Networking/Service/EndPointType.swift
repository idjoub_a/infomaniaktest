//
//  EndPointType.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    //post
    //put ...
}

protocol EndPointType {
    var baseURL: String { get }
    var parameters: [String: Any]? { get }
    var method: HTTPMethod { get }
}

extension EndPointType {
    public var urlRequest: URLRequest {
        guard let url = self.url else {
            fatalError("Error: cannot create URL")
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        return request
    }
    
    private var url: URL? {
        var urlComponents = URLComponents(string: baseURL)
        
        switch method {
        case .get:
            guard let parameters = parameters as? [String: String] else {
                fatalError("Error: cannot add parameters")
            }
            urlComponents?.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        return urlComponents?.url
    }
}
