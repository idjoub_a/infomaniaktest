//
//  Router.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import Foundation

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping (Result<Data, Error>) -> Void)
    func cancel()
}

final class Router<EndPoint: EndPointType>: NetworkRouter {
    
    private var task: URLSessionTask?
    
    func request(_ route: EndPoint, completion: @escaping (Result<Data, Error>) -> Void) {
        let session = URLSession.shared
        task = session.dataTask(with: route.urlRequest, completionHandler: { data, _, error in
            guard let data = data else {
                print("Error\(String(describing: error))")
                completion(.failure(NetworkErrorResponse.noData))
                return
            }
            completion(.success(data))
        })
        if task == nil {
            completion(.failure(NetworkErrorResponse.badRequest))
        }
        self.task?.resume()
    }
    
    func cancel() {
        self.task?.cancel()
    }
}
