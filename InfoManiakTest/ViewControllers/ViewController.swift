//
//  ViewController.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import UIKit
import Kingfisher

enum SearchMode: Int {
    case artist = 0
    case song
}

final class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let manager = ITunesManager()
    private let searchController = UISearchController(searchResultsController: nil)
    private var alertFetch: UIAlertController = UIAlertController(title: nil, message: "Searching...", preferredStyle: .alert)

    private let cellHeight: CGFloat = 450.0
    private var searchMode: SearchMode {
        let index = searchController.searchBar.selectedScopeButtonIndex
        return SearchMode(rawValue: index)!
    }
    
    private let emptyViewXIB = Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)?.first as? EmptyView
    
    private var artistModel: Artist?
    private var songModel: Song?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupSearchBar()
        setupNavBar()
        alertFetch.addAction(UIAlertAction(title: "cancel",
                                                     style: UIAlertAction.Style.default, handler: { [unowned self] _ in
            self.manager.router.cancel()
        }))
    }
    
    private func setTableViewBackground() {
        switch self.searchMode {
        case .artist:
            tableView.backgroundView  = artistModel?.resultCount == 0 ? emptyViewXIB : nil
            tableView.isScrollEnabled = artistModel?.resultCount == 0 ? false : true
        case .song:
            tableView.backgroundView  = songModel?.resultCount == 0 ? emptyViewXIB : nil
            tableView.isScrollEnabled = songModel?.resultCount == 0 ? false : true
        }
    }
    
    private func displayWatchConnectionLoading() {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alertFetch.view.addSubview(loadingIndicator)
        present(alertFetch, animated: true, completion: nil)
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        tableView.register(UINib.init(nibName: DetailsTableViewCell.nibName, bundle: nil),
                           forCellReuseIdentifier: DetailsTableViewCell.cellIdentifier)
    }
    
    private func setupNavBar() {
        self.title = "Music info search 🎶"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = UIColor.init(red: 231.0/255, green: 76.0/255, blue: 60.0/255, alpha: 1.0)
        self.navigationController?.navigationBar.standardAppearance = navBarAppearance
        self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
    }
    
    private func setupSearchBar() {
        
        searchController.searchBar.searchTextField.backgroundColor = .white
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search by name"
        searchController.searchBar.scopeButtonTitles = ["Artist", "Song"]
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        definesPresentationContext = true
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = .white
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
    }

    private func handleSuccessResponse<T: Decodable>(with model: T) {
        if model is Artist {
            self.artistModel = model as? Artist
        } else if model is Song {
            self.songModel = model as? Song
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func fetchModel<T: Decodable>(ofType: T.Type, with searchText: String) {
        displayWatchConnectionLoading()

        let endPoint: ITunesAPI =
            searchMode == .artist ?
            ITunesAPI.artistName(artistName: searchText) : ITunesAPI.trackName(trackName: searchText)
        
        manager.getModel(ofType: T.self,
                         endPoint: endPoint) { [unowned self] (result) in
            switch result {
            case .failure(let error):
                print("Error: \(error)")
                DispatchQueue.main.async {
                    AlertBuilder().displayAlertOk(controller: self, title: "Error", message: error.description)
                }
            case .success(let model):
                print("OK data: \(model)")
                handleSuccessResponse(with: model)
            }
            
            DispatchQueue.main.async {
                self.alertFetch.dismiss(animated: true, completion: {
                    self.setTableViewBackground()
                    if searchMode == .artist ? artistModel?.resultCount == 0 : songModel?.resultCount == 0 {
                        AlertBuilder().displayAlertOk(controller: self, title: "Error", message: "No result")
                    }
                })
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchMode == SearchMode.artist ? artistModel?.results.count ?? 0 : songModel?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: DetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: DetailsTableViewCell.cellIdentifier) as? DetailsTableViewCell
            else { return UITableViewCell() }

        
        if searchMode == SearchMode.artist {
            guard let artistModel = artistModel else {
                return UITableViewCell()
            }
            cell.titleLabel.text = artistModel.results[indexPath.row].artistName
            cell.iTuneImageView.image = UIImage(named: "profilePic")
            cell.firstInfoLabel.text = artistModel.results[indexPath.row].primaryGenreName
            cell.secondInfoLabel.text = ""
            cell.thirdInfoLabel.text = ""
        } else {
            guard let songModel = songModel else {
                return UITableViewCell()
            }
            cell.titleLabel.text = songModel.results[indexPath.row].trackName
            cell.iTuneImageView.kf.setImage(with: URL(string: songModel.results[indexPath.row].artworkUrl100))
            cell.firstInfoLabel.text = songModel.results[indexPath.row].artistName
            cell.secondInfoLabel.text = songModel.results[indexPath.row].kind
            cell.thirdInfoLabel.text = songModel.results[indexPath.row].collectionName
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

extension ViewController: UISearchBarDelegate {
    private func reloadData(with filter: String) {
        if filter.count > 0 {
            switch searchMode {
            case .artist:
                fetchModel(ofType: Artist.self, with: filter)
            case .song:
                fetchModel(ofType: Song.self, with: filter)
            }
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        reloadData(with: searchBar.text ?? "")
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        reloadData(with: searchBar.text ?? "")
    }
}
