//
//  AlertBuilder.swift
//  InfoManiakTest
//
//  Created by Alvaro IDJOUBAR on 16/12/2020.
//

import UIKit

final class AlertBuilder {
    
    public func displayAlertOk(controller: ViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
}
